package CGBVP.emergency.intranet.session_manager.application.port.in;

import CGBVP.emergency.intranet.session_manager.domain.User;

import java.util.Optional;

public interface GetUserUseCase {
    User execute(Long userId);
}
