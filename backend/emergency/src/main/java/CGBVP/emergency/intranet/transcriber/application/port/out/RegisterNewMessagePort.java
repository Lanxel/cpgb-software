package CGBVP.emergency.intranet.transcriber.application.port.out;

import CGBVP.emergency.intranet.transcriber.application.port.in.NewMessageToRegister;
import CGBVP.emergency.intranet.transcriber.domain.Message;

public interface RegisterNewMessagePort {
    Message register(NewMessageToRegister toRegister, Long contentId);
}
