package CGBVP.emergency.intranet.transcriber.domain;

import CGBVP.emergency.intranet.emergency.domain.Emergency;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class Message {
    private long id;
    private Emergency emergency;
    private Content content;
    private User user;
    private String code;
    private String type;
    private boolean deleted;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
