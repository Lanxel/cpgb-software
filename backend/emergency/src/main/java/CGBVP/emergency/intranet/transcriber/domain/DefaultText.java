package CGBVP.emergency.intranet.transcriber.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DefaultText {
    private long id;
    private String text;
    private String code;
}
