package CGBVP.emergency.intranet.session_manager.application.port.out;

import CGBVP.emergency.intranet.session_manager.domain.User;

import java.util.Optional;

public interface GetUserPort {
    User getUser(Long userId);
}
