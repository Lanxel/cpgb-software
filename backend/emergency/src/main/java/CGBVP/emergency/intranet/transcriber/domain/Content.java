package CGBVP.emergency.intranet.transcriber.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Content {
    private long id;
    private DefaultText defaultText;
    private String intranetText;
    private String extranetText;
    private String mediaPath;
}
