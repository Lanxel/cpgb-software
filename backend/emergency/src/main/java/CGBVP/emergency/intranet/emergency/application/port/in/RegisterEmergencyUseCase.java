package CGBVP.emergency.intranet.emergency.application.port.in;

import CGBVP.emergency.intranet.emergency.domain.Emergency;

public interface RegisterEmergencyUseCase {
    Emergency execute(EmergencyToRegister emergencyToRegister);
}
