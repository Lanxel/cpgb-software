package CGBVP.emergency.intranet.logistic.domain;

public enum UnitState {
    DISPONIBLE,
    ATENDIENDO,
    NO_DISPONIBLE,
}
