package CGBVP.emergency.intranet.logistic.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.logistic.application.port.in.RegisterUnitUseCase;
import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.application.port.out.RegisterUnitPort;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class RegisterUnitService implements RegisterUnitUseCase {
    private final RegisterUnitPort registerUnitPort;
    @Override
    public Unit execute(UnitToRegister unitToRegister) {
        return registerUnitPort.register(unitToRegister);
    }
}
