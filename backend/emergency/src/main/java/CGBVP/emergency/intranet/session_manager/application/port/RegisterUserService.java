package CGBVP.emergency.intranet.session_manager.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.AccountToRegister;
import CGBVP.emergency.intranet.session_manager.application.port.in.RegisterAccountUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.RegisterUserUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.UserToRegister;
import CGBVP.emergency.intranet.session_manager.application.port.out.RegisterUserPort;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.transaction.Transactional;

@UseCase
@Transactional
@RequiredArgsConstructor
public class RegisterUserService implements RegisterUserUseCase {
    private final RegisterUserPort registerUserPort;
    private final RegisterAccountUseCase registerAccountUseCase;
    private final PasswordEncoder passwordEncoder;

    @Override
    public User execute(UserToRegister userToRegister) {
        try {
            var user = registerUserPort.register(userToRegister);
            var accountToRegister = new AccountToRegister(user.getId(), userToRegister.getUsername(), passwordEncoder.encode(userToRegister.getPassword()),"CGBVP",userToRegister.getType(),"/public/img/profile-image.png");
            registerAccountUseCase.execute(accountToRegister);
            return user;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }
}
