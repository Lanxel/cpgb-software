package CGBVP.emergency.intranet.session_manager.application.port.in;

import CGBVP.emergency.intranet.session_manager.domain.Role;

import java.util.List;

public interface GetRoleListUseCase {
    List<Role> execute();
}
