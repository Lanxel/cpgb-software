package CGBVP.emergency.intranet.transcriber.application.port.in;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContentToRegister {
    Long defaultTextId;
    String intranetText;
    String extranetText;
    String mediaPath;
}
