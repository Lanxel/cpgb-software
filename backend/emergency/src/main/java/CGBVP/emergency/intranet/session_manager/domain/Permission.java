package CGBVP.emergency.intranet.session_manager.domain;

import lombok.Data;

@Data
public class Permission {
    private long id;
    private String description;
}
