package CGBVP.emergency.intranet.emergency.domain;

import lombok.Data;

@Data
public class EmergencySubType {
    private Long id;
    private String name;
    private String code;
}
