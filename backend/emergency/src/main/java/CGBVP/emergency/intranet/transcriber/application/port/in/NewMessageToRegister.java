package CGBVP.emergency.intranet.transcriber.application.port.in;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewMessageToRegister{
    Long emergencyId;
    Long userId;
    Long defaultTextId;
    String code;
    String type;
    String intranetTranscription;
    String extranetTranscription;
}
