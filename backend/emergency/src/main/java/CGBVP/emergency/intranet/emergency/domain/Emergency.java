package CGBVP.emergency.intranet.emergency.domain;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

@Data
public class Emergency {
    private long id;
    private FireCompany company;
    private Date date;
    private String district;
    private String address;
    private String type;
    private String subType;
    private String description;
    private EmergencyState emergencyState;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private boolean deleted;
}
