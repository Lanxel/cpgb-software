package CGBVP.emergency.intranet.emergency.application.port.out;

import CGBVP.emergency.intranet.emergency.domain.EmergencyType;

import java.util.List;

public interface GetEmergencyTypeListPort {
    List<EmergencyType> getList();
}
