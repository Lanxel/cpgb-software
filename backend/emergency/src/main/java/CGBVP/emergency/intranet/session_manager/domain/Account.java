package CGBVP.emergency.intranet.session_manager.domain;

import lombok.Data;

@Data
public class Account {
    private long id;
    private User user;
    private String username;
    private String password;
    private String domain;
    private String type;
    private String imagePath;
}
