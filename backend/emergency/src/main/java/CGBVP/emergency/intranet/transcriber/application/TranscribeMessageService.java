package CGBVP.emergency.intranet.transcriber.application;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.external.transcribe.BidirectionalStreaming;
import CGBVP.emergency.intranet.transcriber.application.port.in.TranscribeMessageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

@UseCase
@RequiredArgsConstructor
public class TranscribeMessageService implements TranscribeMessageUseCase {
    private final BidirectionalStreaming bidirectionalStreaming;
    @Override
    public String execute(MultipartFile inputAudio) {
        try {
            InputStream inputStream = new BufferedInputStream(inputAudio.getInputStream());
            return bidirectionalStreaming.getTranscription(inputStream);
        } catch (UnsupportedAudioFileException | IOException | ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
