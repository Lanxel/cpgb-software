package CGBVP.emergency.intranet.logistic.application.port.in;

import CGBVP.emergency.intranet.logistic.domain.Unit;

import java.util.List;

public interface GetUnitListUseCase {
    List<Unit> execute(String name);
    List<Unit> execute();
}
