package CGBVP.emergency.intranet.session_manager.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.UpdateUserUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.UserToRegister;
import CGBVP.emergency.intranet.session_manager.application.port.out.UpdateUserPort;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class UpdateUserService implements UpdateUserUseCase {
    private final UpdateUserPort updateUserPort;
    @Override
    public User execute(UserToRegister userToRegister, Long userId) {
        return updateUserPort.update(userToRegister, userId);
    }
}
