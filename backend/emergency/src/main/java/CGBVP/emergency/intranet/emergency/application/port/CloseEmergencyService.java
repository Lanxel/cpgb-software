package CGBVP.emergency.intranet.emergency.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.emergency.application.port.in.CloseEmergencyUseCase;
import CGBVP.emergency.intranet.emergency.application.port.out.CloseEmergencyPort;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class CloseEmergencyService implements CloseEmergencyUseCase {
    public final CloseEmergencyPort closeEmergencyPort;
    @Override
    public void execute(Long emergencyId) {
        closeEmergencyPort.closeEmergency(emergencyId);
    }
}
