package CGBVP.emergency.intranet.session_manager.application.port.in;

import CGBVP.emergency.intranet.session_manager.domain.User;

public interface UpdateUserUseCase {
    User execute(UserToRegister userToRegister, Long userid);
}
