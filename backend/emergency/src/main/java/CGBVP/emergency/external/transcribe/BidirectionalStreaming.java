package CGBVP.emergency.external.transcribe;

import javax.sound.sampled.*;

import CGBVP.emergency.commons.hexagonal.extra.ExternalService;
import CGBVP.emergency.external.awsS3.AwsStorageConfig;
import lombok.RequiredArgsConstructor;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.transcribestreaming.TranscribeStreamingAsyncClient;
import software.amazon.awssdk.services.transcribestreaming.model.*;

import java.io.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@ExternalService
@RequiredArgsConstructor
public class BidirectionalStreaming {
    private static String finalMessage;
    private final AwsStorageConfig awsStorageConfig;

    public String getTranscription(InputStream inputStream) throws UnsupportedAudioFileException, IOException, ExecutionException, InterruptedException {

        Region region = Region.of(awsStorageConfig.getRegion());

        AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(
                awsStorageConfig.getCredentials().getAccessKey(),
                awsStorageConfig.getCredentials().getSecretKey());

        TranscribeStreamingAsyncClient client = TranscribeStreamingAsyncClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
                .region(region)
                .build();

        CompletableFuture<Void> result = client.startStreamTranscription(getRequest(16_000),
                new AudioStreamPublisher(inputStream),
               getResponseHandler());
        result.get();
        client.close();
        return finalMessage;
    }

    private static InputStream getStreamFromMic() throws LineUnavailableException {
        int sampleRate = 16000;
        AudioFormat format = new AudioFormat(sampleRate, 16, 1, true, false);
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

        if (!AudioSystem.isLineSupported(info)) {
            System.out.println("Line not supported");
            System.exit(0);
        }

        TargetDataLine line = (TargetDataLine) AudioSystem.getLine(info);
        line.open(format);
        line.start();

        InputStream audioStream = new AudioInputStream(line);
        return audioStream;
    }
    private static StartStreamTranscriptionResponseHandler getResponseHandler() {
        return StartStreamTranscriptionResponseHandler.builder()
                .onResponse(r -> {
                    System.out.println("Received Initial response");
                })
                .onError(e -> {
                    System.out.println(e.getMessage());
                    StringWriter sw = new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));
                    System.out.println("Error Occurred: " + sw.toString());
                })
                .onComplete(() -> {
                    System.out.println("=== All records stream successfully ===");
                })
                .subscriber(event -> {
                    List<Result> results = ((TranscriptEvent) event).transcript().results();
                    if (results.size() > 0) {
                        if (!results.get(0).alternatives().get(0).transcript().isEmpty()) {
                            System.out.println(results.get(0).alternatives().get(0).transcript());
                            finalMessage = results.get(0).alternatives().get(0).transcript();
                        }
                    }
                })
                .build();
    }

    private static InputStream getStreamFromFile(String myMediaFileName) {
        try {
            File inputFile = new File(myMediaFileName);
            return new FileInputStream(inputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static StartStreamTranscriptionRequest getRequest(Integer mediaSampleRateHertz) {
        return StartStreamTranscriptionRequest.builder()
                .languageCode(LanguageCode.ES_US)
                .mediaEncoding(MediaEncoding.PCM)
                .mediaSampleRateHertz(mediaSampleRateHertz)
                .build();
    }
}
