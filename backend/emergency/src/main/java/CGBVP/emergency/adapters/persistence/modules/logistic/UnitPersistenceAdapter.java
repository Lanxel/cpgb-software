package CGBVP.emergency.adapters.persistence.modules.logistic;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.application.port.out.GetUnitListPort;
import CGBVP.emergency.intranet.logistic.application.port.out.RegisterUnitPort;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import CGBVP.emergency.intranet.logistic.domain.UnitState;
import lombok.RequiredArgsConstructor;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class UnitPersistenceAdapter implements RegisterUnitPort, GetUnitListPort {
    private final UnitModelRepository repository;
    private final UnitMapper mapper;
    @Override
    public Unit register(UnitToRegister unitToRegister) {
        var row = mapper.toModel(unitToRegister);
        row.setState(UnitState.DISPONIBLE.toString());
        return mapper.toUnit(repository.saveAndFlush(row));
    }

    @Override
    public List<Unit> getUnits(String code) {
        return mapper.toUnitList(repository.findAllUnitsOrdered(code));
    }

    @Override
    public List<Unit> getUnits() {
        return mapper.toUnitList(repository.findAllUnitsOrdered(""));
    }
}
