package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.emergency.application.port.in.EmergencyToRegister;
import CGBVP.emergency.intranet.emergency.application.port.out.CloseEmergencyPort;
import CGBVP.emergency.intranet.emergency.application.port.out.GetEmergencyListPort;
import CGBVP.emergency.intranet.emergency.application.port.out.RegisterEmergencyPort;
import CGBVP.emergency.intranet.emergency.domain.Emergency;
import CGBVP.emergency.intranet.emergency.domain.EmergencyState;
import lombok.RequiredArgsConstructor;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class EmergencyPersistenceAdapter implements GetEmergencyListPort , RegisterEmergencyPort, CloseEmergencyPort {
    private final EmergencyModelRepository repository;
    private final EmergencyMapper mapper;
    @Override
    public List<Emergency> getList(String state) {
        return mapper.toEmergencyList(repository.findAllEmergenciesByStatusOrdered(state));
    }

    @Override
    public Emergency register(EmergencyToRegister emergencyToRegister) {
        var row = mapper.toModel(emergencyToRegister);
        //TODO: Change emergency state to ASIGNADO when assign company is implemented
        row.setEmergencyState(String.valueOf(EmergencyState.ATENDIENDO));
        return mapper.toEmergency(repository.saveAndFlush(row));
    }

    @Override
    public void closeEmergency(Long emergencyId) {
        var emergency = repository.findById(emergencyId).orElseThrow(() -> new RuntimeException("Emergency Not Found"));
        emergency.setEmergencyState("CERRADO");
        repository.save(emergency);
    }
}
