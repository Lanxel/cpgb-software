package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.intranet.session_manager.domain.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    @Mapping(target = "name", source = "name")
    @Mapping(target = "id", source = "id")
    Role toRole(RoleModel model);
    List<Role> toRoleList(List<RoleModel> roleModels);
}
