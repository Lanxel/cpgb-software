package CGBVP.emergency.adapters.persistence.modules.session_manager;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "CUENTA")
public class AccountModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cuenta_id")
    private Long id;

    @Column(name = "nombre_usuario",nullable = false)
    private String username;

    @Column(name = "contrasena_hash", nullable = false)
    private String password;

    @Column(name = "dominio",nullable = false)
    private String domain;

    @Column(name = "tipo",nullable = false)
    private String type;

    @Column(name = "imagen")
    private String imagePath;

    @Column(name = "usuario_id", nullable = false)
    private Long userId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id",insertable = false, updatable = false)
    private UserModel user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AccountModel account = (AccountModel) o;
        return id != null && Objects.equals(id, account.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
