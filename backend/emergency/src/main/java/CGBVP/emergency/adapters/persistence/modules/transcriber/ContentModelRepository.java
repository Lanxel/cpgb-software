package CGBVP.emergency.adapters.persistence.modules.transcriber;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ContentModelRepository extends JpaRepository<ContentModel, Long> {
}