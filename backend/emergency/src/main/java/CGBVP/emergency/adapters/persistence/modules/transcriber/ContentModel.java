package CGBVP.emergency.adapters.persistence.modules.transcriber;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "CONTENIDO")
public class ContentModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contenido_id")
    private Long id;

    @Column(name = "texto_intranet")
    private String intranetText;

    @Column(name = "texto_extranet")
    private String extranetText;

    @Column(name = "ubiacion_audio")
    private String mediaPath;

    @Column(name = "texto_predeterminado_id")
    private Long defaultTextId;

    @OneToOne
    @JoinColumn(name = "texto_predeterminado_id",updatable = false, insertable = false)
    private DefaultTextModel defaultText;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ContentModel content = (ContentModel) o;
        return id != null && Objects.equals(id, content.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
