package CGBVP.emergency.adapters.persistence.modules.logistic;

import CGBVP.emergency.adapters.persistence.modules.emergency.FireCompanyMapper;
import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {FireCompanyMapper.class})
public interface UnitMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "state", source = "state")
    @Mapping(target = "firefighterCompany", source = "fireCompany")
    @Mapping(target = "plate", source = "plate")
    @Mapping(target = "imagePath", source = "imagePath")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "type", source = "type")
    Unit toUnit(UnitModel model);
    List<Unit> toUnitList(List<UnitModel> unitModels);

    @Mapping(target = "fireCompanyId", source = "firefighterCompanyId")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "plate", source = "plate")
    @Mapping(target = "imagePath", source = "imagePath")
    UnitModel toModel(UnitToRegister userToRegister);
}
