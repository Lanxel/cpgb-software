package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.intranet.emergency.application.port.in.EmergencyToRegister;
import CGBVP.emergency.intranet.emergency.domain.Emergency;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmergencyMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "date", source = "date")
    @Mapping(target = "district", source = "district")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "subType", source = "subType")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "emergencyState", source = "emergencyState")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "deleted", source = "deleted")
    @Mapping(target = "company", source = "fireCompany")
    Emergency toEmergency(EmergencyModel model);
    List<Emergency> toEmergencyList(List<EmergencyModel> emergencyModels);

    @Mapping(target = "date", source = "date")
    @Mapping(target = "district", source = "district")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "subType", source = "subType")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "companyId", source = "companyId")
    EmergencyModel toModel(EmergencyToRegister toEmergency);
}
