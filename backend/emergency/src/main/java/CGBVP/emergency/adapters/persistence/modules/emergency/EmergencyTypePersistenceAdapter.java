package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.emergency.application.port.out.GetEmergencyTypeListPort;
import CGBVP.emergency.intranet.emergency.domain.EmergencyType;
import lombok.RequiredArgsConstructor;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class EmergencyTypePersistenceAdapter implements GetEmergencyTypeListPort {
    private final EmergencyTypeModelRepository repository;
    private final EmergencyTypeMapper mapper;
    @Override
    public List<EmergencyType> getList() {
        return mapper.toEmergencyTypeList(repository.findAll());
    }
}
