package CGBVP.emergency.adapters.persistence.modules.logistic;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UnitModelRepository extends JpaRepository<UnitModel, Long> {
    @Query(value= "SELECT * FROM unidad where codigo ilike(concat('%',?1,'%')) order by fecha_creacion desc", nativeQuery = true)
    List<UnitModel> findAllUnitsOrdered(String code);
}