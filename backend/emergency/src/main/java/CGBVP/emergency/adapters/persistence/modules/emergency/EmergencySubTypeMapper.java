package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.intranet.emergency.domain.EmergencySubType;
import CGBVP.emergency.intranet.emergency.domain.EmergencyType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmergencySubTypeMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "code", source = "code")
    EmergencySubType toEmergencySubType(EmergencySubType model);
    List<EmergencySubType> toEmergencySubTypeList(List<EmergencySubType> emergencyTypeModels);
}
