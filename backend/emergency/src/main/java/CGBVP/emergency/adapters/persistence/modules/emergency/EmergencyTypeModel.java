package CGBVP.emergency.adapters.persistence.modules.emergency;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "TIPO_EMERGENCIA")
public class EmergencyTypeModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tipo_emergencia_id")
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String name;

    @Column(name = "codigo", nullable = false)
    private String code;

    @OneToMany(mappedBy = "emergencyType")
    private Set<EmergencySubTypeModel> emergencySubTypeList = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        EmergencyTypeModel that = (EmergencyTypeModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
