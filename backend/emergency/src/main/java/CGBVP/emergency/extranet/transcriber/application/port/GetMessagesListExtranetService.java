package CGBVP.emergency.extranet.transcriber.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.extranet.transcriber.application.port.in.GetMessagesListExtranetUseCase;
import CGBVP.emergency.extranet.transcriber.application.port.out.GetMessagesListExtranetPort;
import CGBVP.emergency.intranet.transcriber.application.port.in.GetMessagesListUseCase;
import CGBVP.emergency.intranet.transcriber.application.port.out.GetMessagesListPort;
import CGBVP.emergency.intranet.transcriber.domain.Message;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetMessagesListExtranetService implements GetMessagesListExtranetUseCase {
    private final GetMessagesListExtranetPort getMessagesListExtranetPort;
    @Override
    public List<Message> execute(Long emergencyId) {
        return getMessagesListExtranetPort.getListExtranet(emergencyId);
    }
}
