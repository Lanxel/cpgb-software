package CGBVP.emergency.extranet.transcriber.application.port.in;

import CGBVP.emergency.intranet.transcriber.domain.Message;

import java.util.List;

public interface GetMessagesListExtranetUseCase {
    List<Message> execute(Long emergencyId);
}
