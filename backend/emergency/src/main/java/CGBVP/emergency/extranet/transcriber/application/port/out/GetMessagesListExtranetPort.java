package CGBVP.emergency.extranet.transcriber.application.port.out;

import CGBVP.emergency.intranet.transcriber.domain.Message;

import java.util.List;

public interface GetMessagesListExtranetPort {
    List<Message> getListExtranet(Long emergencyId);
}
