# CGBVP-Software


## Indicaciones para el entorno de desarrollo
### Inicializar la base de datos

Para inicializar el proyecto ejecutar 
el comando en el directorio base.

```
Windows:
docker-compose -f .\docker-compose.database.yml -f .\backend\emergency\docker-compose.yml -f .\web\docker-compose.yml up 

Linux:
docker-compose -f docker-compose.database.yml -f backend\emergency\docker-compose.yml -f web\docker-compose.yml up
```
