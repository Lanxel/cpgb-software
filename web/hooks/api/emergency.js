import {fetcher, useConfig} from './config';
import useSWR from 'swr';
import axios from "axios";

export function getEmergencyList(state) {
  const {apiUrl} = useConfig();

  const {data, error} = useSWR([apiUrl, `emergency/list/${state}`], fetcher);

  return {
    emergencyList: data,
    isLoading: !error && !data,
    error,
  };
}

export function closeEmergency(emergencyId) {
  const {apiUrl} = useConfig();

  return axios.put(`${apiUrl}/emergency/close/${emergencyId}`);
}
export function newEmergency(companyId, type, subType, description, district, address) {
  const {apiUrl} = useConfig();
  let body = {
    "companyId": companyId,
    "type": type,
    "date": new Date(),
    "subType": subType,
    "description": description,
    "district": district,
    "address": address,
  };

  return axios.post(`${apiUrl}/emergency/create`, body);
}