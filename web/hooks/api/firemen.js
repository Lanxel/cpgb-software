import {fetcher, useConfig} from './config';
import useSWR, {useSWRConfig} from 'swr';
import axios from 'axios';

export default function useFireman(firemanId) {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `people/${firemanId}`], fetcher);

  return {
    firemen: data,
    isLoading: !error && !data,
    error,
  };
}

export async function changeFiremanName(firemanId, newName) {
  const {apiUrl} = useConfig();
  const {mutate} = useSWRConfig();

  const updateFireman = (fireman) => axios.put(`${apiUrl}/people/${firemanId}`, {...fireman, name: newName});

  return mutate([apiUrl, `people/${firemanId}`], updateFireman, {revalidate: false});
}
