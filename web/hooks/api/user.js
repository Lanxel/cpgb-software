import {fetcher, useConfig} from './config';
import useSWR from "swr";

export function getUsersList() {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `user/list`], fetcher);

  return {
    usersList: data,
    isLoading: !error && !data,
    error,
  };
}

export function getUsersListByName(name) {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `user/list/${name}`], fetcher);

  return {
    usersList: data,
    isLoading: !error && !data,
    error,
  };
}

export function getUsersById(userId) {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `user/${userId}`], fetcher);

  return {
    user: data,
    isLoading: !error && !data,
    error,
  };
}