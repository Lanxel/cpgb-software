import {useConfig} from './config';
import axios from 'axios';

export async function authUser(username, password) {
  const {apiUrl} = useConfig();

  const body = {
    username: username,
    password: password,
  };

  return await axios.post(`${apiUrl}/auth/login`, body);
}
