import {fetcher, useConfig} from "./config";
import useSWR from "swr";

export function getEmergencyTypeList() {
  const {apiUrl} = useConfig();

  const {data, error} = useSWR([apiUrl, `emergencyType/list`], fetcher);

  return {
    types: data,
    isLoading: !error && !data,
    error,
  };
}