import {fetcher, useConfig} from "./config";
import useSWR from "swr";
import axios from "axios";

export function getUnitsList() {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `unit/list/`], fetcher);

  return {
    unitsList: data,
    isLoading: !error && !data,
    error,
  };
}

export function getUnitsListByCode(code) {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `unit/list/${code}`], fetcher);

  return {
    unitsList: data,
    isLoading: !error && !data,
    error,
  };
}

export function createUnit(type, code, defaultText, message, userId, emergencyId) {
  const {apiUrl} = useConfig();
  let body = {
    "code": code,
    "defaultTextId": defaultText,
    "emergencyId": emergencyId,
    "extranetTranscription": message,
    "intranetTranscription": message,
    "type": type,
    "userId": userId,
  };

  return axios.post(`${apiUrl}/messages/new`, body);
}