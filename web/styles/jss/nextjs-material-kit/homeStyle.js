const homeStyle = (theme) => ({
  topComponents: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    lineHeight: "0px"
  },
  topTabsContainer:{
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: "20px"
  },
  emergencyCardsContainer:{
    marginTop: "20px",
    display: "inherit",
  },
  emergencyCardStyle:{
    [theme.breakpoints.up("md")]: {
      display: "flex",
      flexDirection:"row",
      justifyContent:"space-around"
    },
    display: "flex",
    flexDirection:"column",
    justifyContent:"center",
    alignItems: "center",
  },
  cardsContainer:{
    marginTop: "20px",
    display: "inherit",
  },
  cardFlexContainer:{
    marginTop: "20px",
    display: "flex",
    justifyContent: "center",
  },
  tableContainer:{
    [theme.breakpoints.down("md")]: {
      marginTop: "20px",
      maxWidth: "340px",
    },
    marginTop: "20px",

  },
  tableHeader: {
    backgroundColor: "#C00812",
  }
});

export default homeStyle;