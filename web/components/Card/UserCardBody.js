import React, {useCallback} from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import styles from "styles/jss/nextjs-material-kit/components/cardBodyStyle.js";
import CardBody from "./CardBody";
import {primaryColorVariant} from "../../styles/jss/nextjs-material-kit";
import Button from "../CustomButtons/Button";
import Primary from "../Typography/Primary";
import Link from "next/link";

const useStyles = makeStyles(styles);

export default function UserCardBody(props) {
  const classes = useStyles();
  const { className, children, data, ...rest } = props;
  const cardBodyClasses = classNames({
    [classes.userCardBody]: true,
    [className]: className !== undefined,
  });

  const link = `/edit-user/${data.id}`;
  const date = new Date(data.dateBirth).toLocaleDateString();
  return (
      <div className={cardBodyClasses} {...rest}>
        <CardBody className={classes.userBody}>
          <div className={classes.profileImageContainer}>
            <img className={classes.profileImage}
                 src="/img/profile-image.png"
                 alt={""}></img>
            <div className={classes.title} style={{color:primaryColorVariant}}>{data.name}</div>
            <div>{data.position}</div>
          </div>
          <div className={classes.flexContain}>
            <Primary>CORREO: </Primary>
            {data.email}
          </div>
          <div className={classes.flexContain}>
            <Primary>CELULAR: </Primary>
            {data.contactNumber}</div>
          <div className={classes.flexContain}>
            <Primary>DNI: </Primary>
            {data.dni}
          </div>
          <div className={classes.flexContain}>
            <Primary>GENERO: </Primary>
            {data.gender}
          </div>
          <div className={classes.flexContain}>
            <Primary>TIPO DE SANGRE: </Primary>
            {data.bloodType}
          </div>
          <div className={classes.flexContain}>
            <Primary>FECHA DE NACIMIENTO: </Primary>
            {date}
          </div>
          <div className={classes.flexContain}>
            <Primary>ROL: </Primary>
            {data.role}
          </div>
          <Link href={link}>
            <Button color="primary" style={{margin: "1rem 2rem"}}>Editar información</Button>
          </Link>
          {children}
        </CardBody>
      </div>
  );
}

UserCardBody.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  data: PropTypes.object,
};
