import React, {useState} from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
// @material-ui/icons
// core components
import styles from "styles/jss/nextjs-material-kit/components/cardBodyStyle.js";

import CardBody from "./CardBody";
import {primaryColorVariant} from "../../styles/jss/nextjs-material-kit";
import GridItem from "../Grid/GridItem";
import GridContainer from "../Grid/GridContainer";
import Select from '@material-ui/core/Select';
import {ListItem, TextField} from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";
import Button from "../CustomButtons/Button";
import ListItemText from "@material-ui/core/ListItemText";
import {SearchOutlined} from "@material-ui/icons";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import Recorder from "../Recorder/Recorder";
import {getDefaultTextList, getDefaultTextListByText, newMessagesList} from "../../hooks/api/messages";
import List from "@material-ui/core/List";
import {useRouter} from "next/router";
import {getCookie} from "cookies-next";

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

export default function NewMessageCardBody(props) {
    const classes = useStyles();
    const router = useRouter();
    const cookie = getCookie("user");
    const user = JSON.parse(cookie??"{}");
    const {id: emergencyId} = router.query;
    const [transcription, setTranscription] = useState("");
    const [transcriptionStatus, setTranscriptionStatus] = useState("");
    const { className, children, ...rest } = props;
    const cardBodyClasses = classNames({
        [classes.cardBodyForm]: true,
        [className]: className !== undefined,
    });

    const [state, setState] = React.useState({
        type: '',
        name: 'hai',
    });

    const [selectedEnabled, setSelectedEnabled] = React.useState("");
    const wrapperDiv = classNames(
        classes.checkboxAndRadio,
        classes.checkboxAndRadioHorizontal,
    );
    const [searchString, setSearchString] = useState("");
    const {defaultTextList, isLoading} = searchString!==""? getDefaultTextListByText(searchString): getDefaultTextList();
    const [defaultTextSelected, setDefaultTextSelected] = React.useState(false);
    const [defaultText, setDefaultText] = React.useState("");
    const [defaultTextId, setDefaultTextId] = React.useState(null);
    const [codeValue, setCodeValue] = useState("");
    const renderDefaultText = selectedEnabled === "a";
    const renderVoiceView = selectedEnabled === "b";
    const defaultTextView = (
        <>
            { !defaultTextSelected && <GridItem style={{display:"inherit", justifyContent:"center", alignItems:"end"}}>
                <SearchOutlined style={{alignSelf:"center"}}/>
                <TextField id="search-input" label="Buscar" variant="filled" color={"secondary"} onChange={(event) => setSearchString(event.target.value)} />
            </GridItem>}
            <GridItem>
                <div className={classes.listContainer}>
                    <List component="nav" aria-label="default-text">
                        {!isLoading && !defaultTextSelected && defaultTextList.map((item) => (
                            <ListItem
                                button
                                style={{borderBottom: "solid 0.5px", backgroundColor:"grey", width:"100%"}}
                                key={item.id}
                                onClick={() => {setDefaultTextSelected(true); setDefaultText(item.text); setDefaultTextId(item.id);}}
                            >
                                <ListItemText primary={item.text} />
                            </ListItem>
                        ))}
                        {defaultTextSelected &&
                            <TextField
                                id="filled-multiline-static"
                                label="Predeterminado"
                                multiline
                                rows={6}
                                disabled
                                defaultValue={defaultText}
                                variant="filled"
                                color={"secondary"}
                            />
                        }
                    </List>

                </div>
            </GridItem>
        </>
    );


    const voiceRender = (
        <div style={{display:"inherit", flexDirection:"column", minWidth:"30%"}}>
            <Recorder onSubmit={(arg) => {setTranscription(arg);}} onRecord={(arg) => {setTranscriptionStatus(arg);}} transcriptionStatus={transcriptionStatus} />
        </div>
    );

    const handleChange = (event) => {
        const name = event.target.name;
        setState({
            ...state,
            [name]: event.target.value,
        });
    };

    const [modal, setModal] = React.useState(false);

    const handleRequest = () => {
        const userId = user.id;
        const res = newMessagesList(state.type, codeValue, defaultTextId,transcription, userId, emergencyId);
        router.push({pathname: "/emergency/" + emergencyId, query: {state: "ATENDIENDO"}});
        return res;
    };

    const modalComponent = (
        <Dialog
            open={modal}
            TransitionComponent={Transition}
            keepMounted
            onClose={() => setModal(false)}
            aria-labelledby="modal-slide-title"
            aria-describedby="modal-slide-description"
            style={{minWidth:"300"}}
        >
            <DialogContent
                id="modal-slide-description"
                style={{padding: "1.5rem 2rem", fontWeight: "600", textAlign:"center"}}
            >
                ¿Seguro que quiere enviar el mensaje?
            </DialogContent>
            <DialogActions
                style={{justifyContent:"center"}}
            >
                <Button onClick={() => setModal(false)}>Cancelar</Button>
                <Button color="primary" onClick={handleRequest}>
                    Aceptar
                </Button>
            </DialogActions>
        </Dialog>
    );

    return (
        <div className={cardBodyClasses} {...rest}>
            <CardBody className={classes.newEmergencyBody}>
                <div className={classes.title} style={{color:primaryColorVariant, paddingTop: "15px"}}>Nuevo mensaje</div>
                <GridContainer style={{justifyContent: "center", width: "inherit", paddingTop: "15px"}}>
                    <div style={{flexDirection: "row", width: "250px"}}>
                        <FormControl required className={classes.formControl} style={{padding: "0px 15px"}}>
                            <InputLabel htmlFor="type-native-required" color="secondary" style={{padding: "0px 15px"}}>Tipo</InputLabel>
                            <Select
                                native
                                value={state.type}
                                onChange={handleChange}
                                name="type"
                                inputProps={{
                                    id: 'type-native-required',
                                }}
                                color="secondary"
                            >
                                <option aria-label="None" value="" />
                                <option value={"General"}>General</option>
                                <option value={"Alerta"}>Alerta</option>
                            </Select>
                        </FormControl>
                    </div>
                    <div style={{flexDirection: "row", width: "250px"}}>
                        <GridItem>
                            <TextField required onChange={(newValue) => setCodeValue(newValue.target.value)} id="standard-required" label="Código" color="secondary" style={{width: "100%"}}/>
                        </GridItem>
                    </div>
                    <GridItem>
                        <div className={classes.label} style={{marginTop: "20px"}}>Tipo mensaje:</div>
                    </GridItem>
                    <GridItem style={{display: "inherit", justifyContent: "center", marginBottom:"15px"}}>
                        <div className={wrapperDiv}>
                            <FormControlLabel style={{padding: "0px 15px"}}
                                control={
                                    <Radio
                                        checked={selectedEnabled === "a"}
                                        onChange={() => setSelectedEnabled("a")}
                                        value="a"
                                        name="radio button enabled"
                                        aria-label="A"
                                        icon={
                                            <FiberManualRecord
                                                className={classes.radioUnchecked}
                                            />
                                        }
                                        checkedIcon={
                                            <FiberManualRecord className={classes.radioChecked} />
                                        }
                                        classes={{
                                            checked: classes.radio
                                        }}
                                    />
                                }
                                classes={{
                                    label: classes.label
                                }}
                                label="Predeterminado"
                            />
                            <FormControlLabel style={{padding: "0px 15px" }}
                                control={
                                    <Radio
                                        checked={selectedEnabled === "b"}
                                        onChange={() => setSelectedEnabled("b")}
                                        value="b"
                                        name="radio button enabled"
                                        aria-label="B"
                                        icon={
                                            <FiberManualRecord
                                                className={classes.radioUnchecked}
                                            />
                                        }
                                        checkedIcon={
                                            <FiberManualRecord className={classes.radioChecked} />
                                        }
                                        classes={{
                                            checked: classes.radio
                                        }}
                                    />
                                }
                                classes={{
                                    label: classes.label
                                }}
                                label="Voz"
                            />
                        </div>
                    </GridItem>
                    {renderDefaultText && defaultTextView || renderVoiceView && voiceRender}
                    <GridItem style={{marginTop: "2rem"}}>
                        <Button style={{marginRight:"10px"}}>Cancelar</Button>
                        <Button color="primary" disabled={!(transcriptionStatus==="Finished" || defaultTextId)} onClick={() => setModal(true)}>Publicar</Button>
                    </GridItem>
                </GridContainer>
                {children}
            </CardBody>
            {modalComponent}
        </div>
    );
}

NewMessageCardBody.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
};
