import React, {useCallback} from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import styles from "styles/jss/nextjs-material-kit/components/cardBodyStyle.js";
import CardBody from "./CardBody";
import {primaryColorVariant} from "../../styles/jss/nextjs-material-kit";
import Button from "../CustomButtons/Button";
import GridContainer from "../Grid/GridContainer";
import {TextField} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";

const useStyles = makeStyles(styles);

export default function NewUserFormCardBody(props) {
  const classes = useStyles();
  const { className, children, ...rest } = props;
  const cardBodyClasses = classNames({
    [classes.userCardBody]: true,
    [className]: className !== undefined,
  });

  const [selectedEnabled, setSelectedEnabled] = React.useState("");


  const gender = (
      <div style={{display: "flex", flexDirection:"row"}}>
        <FormControlLabel style={{padding: "0px 15px"}}
                          control={
                            <Radio
                                checked={selectedEnabled === "Masculino"}
                                onChange={() => setSelectedEnabled("Masculino")}
                                value="Masculino"
                                name="radio button enabled"
                                aria-label="Masculino"
                                icon={
                                  <FiberManualRecord
                                      className={classes.radioUnchecked}
                                  />
                                }
                                checkedIcon={
                                  <FiberManualRecord className={classes.radioChecked} />
                                }
                                classes={{
                                  checked: classes.radio
                                }}
                            />
                          }
                          classes={{
                            label: classes.label
                          }}
                          label="Masculino"
        />
        <FormControlLabel style={{padding: "0px 15px" }}
                          control={
                            <Radio
                                checked={selectedEnabled === "Femenino"}
                                onChange={() => setSelectedEnabled("Femenino")}
                                value="Femenino"
                                name="radio button enabled"
                                aria-label="Femenino"
                                icon={
                                  <FiberManualRecord
                                      className={classes.radioUnchecked}
                                  />
                                }
                                checkedIcon={
                                  <FiberManualRecord className={classes.radioChecked} />
                                }
                                classes={{
                                  checked: classes.radio
                                }}
                            />
                          }
                          classes={{
                            label: classes.label
                          }}
                          label="Femenino"
        />
      </div>
  );
  return (
      <div className={cardBodyClasses} {...rest}>
        <CardBody className={classes.userFormBody}>
          <div className={classes.title} style={{color:primaryColorVariant, paddingTop: "15px"}}>Nuevo mensaje</div>
          <GridContainer style={{justifyContent: "center"}}>
            <div className={classes.labelClass}>
              <TextField required id="names-required" label="Nombres" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="lastname-required" label="Apellidos" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="names-required" label="Numero de Celular" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="lastname-required" label="DNI" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass} style={{display:"flex", flexDirection:"column", justifyContent:"center", paddingLeft:"2rem"}}>
              <div style={{alignSelf: "start", }}>Genero</div>
              {gender}
            </div>
            <div className={classes.labelClass}>
              <TextField required id="lastname-required" label="Tipo de Sangre" color="secondary"  style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="lastname-required" label="Cargo" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="lastname-required" label="Fecha de nacimiento" color="secondary"  style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="lastname-required" label="Rol" color="secondary" style={{width:"80%"}}/>
            </div>
          </GridContainer>
          <Button color="primary" style={{margin: "1rem 2rem"}}>Editar información</Button>
          {children}
        </CardBody>
      </div>
  );
}

NewUserFormCardBody.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};
