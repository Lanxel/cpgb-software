import Head from "next/head";
import React from "react";
import HeaderCGBVP from "../Header/HeaderCGBVP";
import PropTypes from "prop-types";
import Card from "../Card/Card";

// eslint-disable-next-line react/prop-types
export default function PageLayout( props){
  const { children, title, data, ...rest } = props;
  return(
      <>
        <Head>
          <meta
              name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <title>{title}</title>
          <link rel="icon" href={"/favicon.ico"}/>
        </Head>
        <HeaderCGBVP
            color="primary"
            changeColorOnScroll={{
              height: 400,
              color: "white",
            }}
            data={data}
        >
          {children}
        </HeaderCGBVP>
      </>
  );
}

PageLayout.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
  data: PropTypes.object,
};