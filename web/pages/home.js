import React from "react";
// nodejs library that concatenates classes
//import classNames from "classnames";
// react components for routing our app without refresh
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import styles from "../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../components/PageLayout/PageLayout";
import {Container} from "@material-ui/core";
import Button from "../components/CustomButtons/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import EmergencyCardBody from "../components/Card/EmergencyCardBody";
import Card from "../components/Card/Card";
import Link from "next/link";
import Primary from "../components/Typography/Primary";
import {getEmergencyList} from "../hooks/api/emergency";

const useStyles = makeStyles(styles);


export default function Home() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const state = value? "CERRADO": "ATENDIENDO";
  const {emergencyList, isLoading, error} = getEmergencyList(state);
  return (
      <div>
        <PageLayout title={"Home"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>LISTA DE EMERGENCIAS</h4>
              </Primary>
              <Link href={"/emergency/create"}>
                <Button style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}}>+ Nueva Emergencia</Button>
              </Link>
            </div>
            <div className={classes.topTabsContainer}>
              <Tabs
                  value={value}
                  indicatorColor="secondary"
                  onChange={handleChange}
              >
                <Tab label="En progreso" />
                <Tab label="Finalizados" />
              </Tabs>
            </div>
            <div className={classes.emergencyCardsContainer}>
              <div className={classes.emergencyCardStyle}>
                { !isLoading && emergencyList.map((value) =>
                    <Card style={{maxWidth:"300px"}} key={value.id}>
                      <EmergencyCardBody data={value}></EmergencyCardBody>
                    </Card>
                    )
                }
              </div>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}
