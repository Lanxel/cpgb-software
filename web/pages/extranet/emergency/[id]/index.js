import React from "react";

import {makeStyles} from "@material-ui/core/styles";
import styles from "../../../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayoutExtranet from "../../../../components/PageLayout/PageLayoutExtranet";
import Primary from "../../../../components/Typography/Primary";
import {Container} from "@material-ui/core";
import TimeLineExtranet from "../../../../components/TimeLine/TimeLine";
import {getMessagesExtranetList} from "../../../../hooks/api/messages";
import {useRouter} from "next/router";
import Divider from "@material-ui/core/Divider";



const useStyles = makeStyles(styles);


export default function emergencyPage() {
  const classes = useStyles();
  const router = useRouter();
  const {id: emergencyId, state: emergencyState} = router.query;
  if (!emergencyId) {
    return (
        <div>ERROR</div>
    );
  }

  const {messagesList, error, isLoading} = getMessagesExtranetList(emergencyId);

  return (
      <div>
        <PageLayoutExtranet title={"Emergencias"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>LISTA DE MENSAJES</h4>
              </Primary>
            </div>
            <div style={{justifyContent: "initial"}}>
              { emergencyState==="CERRADO" &&
                  <div style={{textAlign: "center"}}>
                    <Primary><h2>La Emergencia ha concluido</h2></Primary>
                    <Divider style={{marginTop: "20px", height: "3px"}}/>
                  </div>
              }
              <TimeLineExtranet data={messagesList} loading={isLoading}></TimeLineExtranet>
            </div>
          </Container>
        </PageLayoutExtranet>
      </div>
  );
}