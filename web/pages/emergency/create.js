import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../components/PageLayout/PageLayout";
import {Container} from "@material-ui/core";
import Button from "../../components/CustomButtons/Button";
import Card from "../../components/Card/Card";
import Link from "next/link";
import NewEmergencyFormCardBody from "../../components/Card/NewEmergencyFormCardBody";
import {useRouter} from "next/router";
import Primary from "../../components/Typography/Primary";

const useStyles = makeStyles(styles);


export default function CreateEmergency() {
  const classes = useStyles();
  return (
      <div>
        <PageLayout title={"Home"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>CREAR EMERGENCIA</h4>
              </Primary>
              <Link href={"/home"}>
                <Button style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}}>Volver</Button>
              </Link>
            </div>
            <div className={classes.cardFlexContainer}>
              <Card style={{maxWidth: "700px"}}>
                <NewEmergencyFormCardBody></NewEmergencyFormCardBody>
              </Card>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}